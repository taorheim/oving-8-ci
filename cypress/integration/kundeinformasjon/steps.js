import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
  cy.visit('http://localhost:8080');

  cy.get('#product').select('Hobby');
  cy.get('#quantity').clear().type('2');
  cy.get('#saveItem').click();

  cy.get('#product').select('Hubba bubba');
  cy.get('#quantity').clear().type('4');
  cy.get('#saveItem').click();
});

And(/^trykket på Gå til betaling$/, () => {
  cy.get('#goToPayment').click();
})

When(/^jeg legger inn navn, adresse, postnummer, poststed og kortnummer$/, () => {
  cy.get('#fullName').clear().type("Tore Orheim");
  cy.get('#address').clear().type("Anne Hogstads Veg 18");
  cy.get('#postCode').clear().type("7046");
  cy.get('#city').clear().type("Trondheim");
  cy.get('#creditCardNo').clear().type("1925485658747741");
  
});

And(/^trykker på Fullfør kjøp$/, () => {
  cy.get('input').contains('Fullfør handel').click();
})

Then(/^skal jeg få beskjed om at kjøpet er registrert$/, () => {
  cy.get('div').should('contain', 'Din ordre er registrert')
});

Given(/^at jeg har lagt inn varer i handlekurven$/, () => {
  cy.visit('http://localhost:8080');

  cy.get('#product').select('Hobby');
  cy.get('#quantity').clear().type('2');
  cy.get('#saveItem').click();

  cy.get('#product').select('Hubba bubba');
  cy.get('#quantity').clear().type('4');
  cy.get('#saveItem').click();
});

And(/^trykket på Gå til betaling$/, () => {
  cy.get('#goToPayment').click();
})

When(/^jeg legger inn ugyldige verdier i feltene$/, () => {
  cy.get('#fullName').clear()
  cy.get('#address').clear()
  cy.get('#postCode').clear()
  cy.get('#city').clear()
  cy.get('#creditCardNo').clear().type("19254856");
  
  cy.get('input').contains('Fullfør handel').click();
})

Then(/^skal jeg få feilmeldinger for disse$/, () => {
  cy.get('div > #fullNameError').should('contain', 'Feltet må ha en verdi');
  cy.get('div > #addressError').should('contain', 'Feltet må ha en verdi');
  cy.get('div > #postCodeError').should('contain', 'Feltet må ha en verdi');
  cy.get('div > #cityError').should('contain', 'Feltet må ha en verdi');
  cy.get('div > #creditCardNoError').should('contain', 'Kredittkortnummeret må bestå av 16 siffer');
})
