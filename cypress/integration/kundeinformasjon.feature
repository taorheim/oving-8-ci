# language: no
Egenskap: Kundeinformasjon

  Som kunde av nettkiosken
  Ønsker jeg å kunne legge inn betalingsinformasjon
  Slik at jeg kan få godis i posten

  Scenario: Fullført kjøp
    Gitt at jeg har lagt inn varer i handlekurven
    Og trykket på Gå til betaling
    Når jeg legger inn navn, adresse, postnummer, poststed og kortnummer
    Og trykker på Fullfør kjøp
    Så skal jeg få beskjed om at kjøpet er registrert

  Scenario: Ugyldige verdier i betalingsside
    Gitt at jeg har lagt inn varer i handlekurven
    Og trykket på Gå til betaling
    Når jeg legger inn ugyldige verdier i feltene
    Så skal jeg få feilmeldinger for disse 