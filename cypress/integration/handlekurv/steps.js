import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

Given(/^at jeg har åpnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

When(/^jeg legger inn varer og kvanta$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

Then(/^skal handlekurven inneholde det jeg har lagt inn$/, () => {
    cy.get('#list > li').should(($lis) => {
        expect($lis).to.have.length(4);
        expect($lis.eq(0)).to.contain('1 Stratos; 8 kr');
        expect($lis.eq(1)).to.contain('4 Hubba bubba; 8 kr');
        expect($lis.eq(2)).to.contain('5 Smørbukk; 5 kr');
        expect($lis.eq(3)).to.contain('2 Hobby; 12 kr');
      });
});

And(/^den skal ha riktig totalpris$/, function () {
        cy.get('#price').should('have.text', '33');
});

Given(/^at jeg har opnet nettkiosken$/, () => {
    cy.visit('http://localhost:8080');
});

And(/^jeg har lagt inn varer og kvanta$/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
});

When(/^jeg sletter varer/, () => {
    cy.get('#product').select('Smørbukk');
    cy.get('#deleteItem').click();

    cy.get('#product').select('Hubba bubba');
    cy.get('#deleteItem').click();
})

Then(/^skal handlekurven ikke inneholde det jeg slettet$/, () => {
    cy.get('#list > li').should(($lis) => {
        expect($lis).to.have.length(2);
        expect($lis.eq(0)).to.contain('1 Stratos; 8 kr');
        expect($lis.eq(1)).to.contain('2 Hobby; 12 kr');
      });
})

Given(/^at jeg har åpnet nettkiosken/, () => {
    cy.visit('http://localhost:8080');
})

And(/^jeg har lagt inn varer og kvanta/, () => {
    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('4');
    cy.get('#saveItem').click();

    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('5');
    cy.get('#saveItem').click();

    cy.get('#product').select('Stratos');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();

    cy.get('#product').select('Hobby');
    cy.get('#quantity').clear().type('2');
    cy.get('#saveItem').click();
})

When(/^jeg oppdaterer kvanta for en vare/, () => {
    cy.get('#product').select('Smørbukk');
    cy.get('#quantity').clear().type('3')
    cy.get('#saveItem').click();

    cy.get('#product').select('Hubba bubba');
    cy.get('#quantity').clear().type('1');
    cy.get('#saveItem').click();
})

Then(/^skal handlekurven inneholde riktig kvanta for varen/, () => {
    cy.get('#list > li').should(($lis) => {
        expect($lis).to.have.length(4);
        expect($lis.eq(0)).to.contain('1 Stratos; 8 kr');
        expect($lis.eq(1)).to.contain('1 Hubba bubba; 2 kr');
        expect($lis.eq(2)).to.contain('3 Smørbukk; 3 kr');
        expect($lis.eq(3)).to.contain('2 Hobby; 12 kr');
      });
})